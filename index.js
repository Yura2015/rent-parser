(async function init() {
  const URL = require('url')
  const axios = require('axios').default
  const csv = require('csv-parser')
  const beep = require('beepbeep')
  const fs = require('fs')
  const {getDealString, getPrice} = require('./deal-parser')

  fileURL = process.argv.slice(-1)[0]

  const gid = fileURL.split('gid=')[1]
  const id = URL.parse(fileURL).path.split('/').slice(-2)[0]
  const downloadUrl = `https://docs.google.com/spreadsheets/d/${id}/export?format=csv&id=${id}&gid=${gid}`

  

  try {
    const response = await axios.get(downloadUrl, {responseType: 'stream'});
    const stream = response.data
    const results = []
    
    const fileStream = fs.createWriteStream('new.txt')
    stream.pipe(csv())
    .on('data', data => {
      if(getPrice(data['Sale Price']) > 1000000 ) {
        results.push(data)
        fileStream.write(getDealString(data))
      }
    })
    
    .on('end', () => {
      // console.log(results)
      fileStream.close()
    })
  } catch(error) {
    console.log(error)
  }
  
})()
