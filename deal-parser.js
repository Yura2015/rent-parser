function trimKeys(obj) {
  for (key in obj) {
    const trimmedKey = key.trim()
    if (trimmedKey !== key) {
      obj[trimmedKey] = obj[key]
      delete obj[key]
    }
  }
}
function removeSeparators(string) {
  return string.replace(/ /g, '').replace(/,/g, '').replace(/\./g, '').replace(/&/g, ' #').replace(/And/g, ' #')
}

function capitalize(input) { 
  var words = input.split(' ').map(x => x.toLowerCase());  
  var CapitalizedWords = [];  
  words.forEach(element => {  
      if (!element) {
        return null
      }
      CapitalizedWords.push(element[0].toUpperCase() + element.slice(1, element.length));  
  });  
  return CapitalizedWords.join(' ').replace(/\n/g, ' ');  
} 

function transformDate(date) {
  if (!date || date === 0) {
    return ''
  }
  const [month,day,year] = date.split('/')
  const newMonth = month.length === 1 ? '0' + month :  month 
  const newDay = day.length === 1 ? '0' + day : day 

  return `${newMonth}/${newDay}/${year}`
}

function getLastSaleDate(date) {
  return date ? ` (${transformDate(date)})` : ''
}

function splitNumberWithCommas(number) {
  let firstPart = Math.floor(number / 1000)
  if (firstPart === 0) {
    return `${number}`
  }
  if (firstPart >= 1000) {
    firstPart = splitNumberWithCommas(firstPart)
  }
  let secondPart  = `${number % 1000}`
  if (secondPart.length === 1) {
    secondPart = `00${secondPart}`
  }
  if (secondPart.length === 2) {
    secondPart = `0${secondPart}`
  }
  return `${firstPart},${secondPart === 0 ? '000' : secondPart}`
}

function getAddress(address) {
  const newAddress = address.split(',')[0].split('#')[0].replace(' -', '-')
  return capitalize(newAddress)
}

const getPrice = (str) => {
  if (!str) return ''
  return Number(str.replace(/\$/g, '').replace(/,/g, ''))
}

function getAssetType(string) {
  if (!string) {
    return ''
  }

  const triggerPhrases = {
    'Condominiums' : 'Condo',
    'Condo' : 'Condo',
    'Apartments' : 'Multifamily',
    'Store' : 'Retail',
    'Retail' : 'Retail',
    'Frame' : 'SFR',
    'One Family' : 'SFR',
    'Single Family' : 'SFR',
    '2-4 Family' : 'Multifamily',
    'Two Family' : 'Multifamily',
    'Warehouses' : 'Industrial',
    'Manufacturing' : 'Industrial',
    'Warehouses' : 'Industrial',
    'Office' : 'Office',
    'Warehousing': 'Industrial',
    'Families': 'Multifamily',
    'With stores': 'Mixed-use',
    'hotel': 'Hotel',
    'or office': 'Mixed-use'
  }

  for (triggerPhrase in triggerPhrases) {
    if (string.search(triggerPhrase) !== -1) {
      return triggerPhrases[triggerPhrase]
    }
  }
  
  return ''
}


const keys = {
  ContractDate : 'Contract Date',
  Address : 'Address',
  Market : 'Market',
  BuildingClass : 'Building Class',
  SalePrice : 'Sale Price',
  BSF : 'BSF',
  SF : 'SF',
  Buyer : 'Buyer',
  Seller : 'Seller',
  LastSalePrice : 'Last Sale Price',
  Zoning: 'Zoning',
  LotDimensions: 'Lot dimensions',
  LastSaleDate : 'Last Sale Date',
  Units: 'Units'
}

function getDealString(deal) {
  trimKeys(deal)
  return `
SALE
IMAGE: 
DATE: ${transformDate(deal[keys.ContractDate])}
ADDRESS: ${getAddress(deal[keys.Address])}${!!deal[keys.Market] && `
MARKET: ${capitalize(deal[keys.Market])}` || ''}
ASSET TYPE: ${getAssetType(deal[keys.BuildingClass])}

BUYER: ${capitalize(deal[keys.Buyer])}
SELLER: ${capitalize(deal[keys.Seller])}

PRICE: ${deal[keys.SalePrice]}${getPrice(deal[keys.BSF]) && `
BSF: ${splitNumberWithCommas(getPrice(deal[keys.BSF]))} ~ PPBSF: $${splitNumberWithCommas(Math.round(getPrice(deal[keys.SalePrice]) / getPrice(deal[keys.BSF])))}` || ''}${!!getPrice(deal[keys.SF]) && `
SF: ${splitNumberWithCommas(getPrice(deal[keys.SF]))} ~ PPSF: $${splitNumberWithCommas(Math.round(getPrice(deal[keys.SalePrice]) / getPrice(deal[keys.SF])))}` || ''}${!!deal[keys.Units] && `
UNITS: ${splitNumberWithCommas(getPrice(deal[keys.Units]))} ~ PPU: $${splitNumberWithCommas(Math.round(getPrice(deal[keys.SalePrice]) / getPrice(deal[keys.Units])))}` || ''}${!!deal[keys.Zoning] || !!deal[keys.LotDimensions] || !!deal[keys.LastSalePrice] ? `
` : ''}${!!deal[keys.Zoning] && `
ZONING: ${deal[keys.Zoning]}` || ''}${!!deal[keys.LotDimensions] && `
LOT DIMENSIONS: ${deal[keys.LotDimensions]}` || ''}${!!deal[keys.LastSalePrice] && `
LAST SALE PRICE: ${splitNumberWithCommas(getPrice(deal[keys.LastSalePrice]))}${getLastSaleDate(deal[keys.LastSaleDate])}` || ''}

=SOURCE: Transfer list

#NewYork #RealEstate #tradedny #NY${deal[keys.Market] ? ` #${removeSeparators(capitalize(deal[keys.Market]))}` : ''} #${removeSeparators(capitalize(deal[keys.Buyer]))} #${removeSeparators(capitalize(deal[keys.Seller]))}
`
}

module.exports = {
  getDealString,
  getPrice
}